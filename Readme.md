## Dotefiles

```
├── alacritty
│  ├── alacritty.yml
│  └── dracula.yml
├── awesome
│  ├── 600
│  ├── freedesktop
│  ├── ISSUE_TEMPLATE.md
│  ├── lain
│  ├── rc.lua
│  ├── rc.lua.template
│  ├── README.rst
│  ├── scripts
│  └── themes
├── cmus
│  ├── autosave
│  ├── cache
│  ├── command-history
│  ├── Dracula.theme
│  ├── lib.pl
│  ├── playlists
│  └── search-history
├── dunst
│  ├── alert.sh
│  ├── dunstrc
│  ├── dunstrc_old
│  └── mixkit-message-pop-alert-2354.wav
├── i3
│  └── config
├── neofetch
│  ├── config.conf
│  └── config.old
├── packages
│  ├── desktop-programs.txt
│  └── man-apt-package.txt
├── polybar
│  ├── cmus.sh
│  ├── config
│  ├── launch_i3.sh
│  └── launch_multimon.sh
├── qutebrowser
│  ├── autoconfig.yml
│  ├── bookmarks
│  ├── config.py
│  ├── dracula
│  └── quickmarks
├── Readme.md
├── screenshots
│  ├── Screenshot_from_2020-09-24_22-22-28.png
│  ├── Screenshot_from_2020-09-24_23-41-55.png
│  └── Screenshot_from_2020-09-24_23-42-12.png
├── scripts
│  ├── autostart.sh
│  ├── copy_dotefile.sh
│  ├── package_list.sh
│  ├── screenshot.sh
│  ├── ubuntu-post_install.sh
│  └── wallpaper.sh
├── Setup.md
├── starship.toml
├── vifm
│  ├── colors
│  ├── scripts
│  ├── vifm-help.txt
│  ├── vifminfo
│  └── vifmrc
└── zathura
   └── zathurarc

```

