-- vim: ts=4 sw=4 noet ai cindent syntax=lua
--[[
Conky, a system monitor, based on torsmo

Any original torsmo code is licensed under the BSD license

All code written since the fork of torsmo is licensed under the GPL

Please see COPYING for details

Copyright (c) 2004, Hannu Saransaari and Lauri Hakkarainen
Copyright (c) 2005-2012 Brenden Matthews, Philip Kovacs, et. al. (see AUTHORS)
All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]
conky.config = {
	
	update_interval = 1,
	cpu_avg_samples = 2,
	net_avg_samples = 2,
	out_to_console = false,
	override_utf8_locale = true,
	double_buffer = true,
	no_buffers = true,
	text_buffer_size = 32768,
	imlib_cache_size = 0,
	own_window = true,
	own_window_type = 'normal',
	own_window_argb_visual = true,
	own_window_argb_value = 50,
	own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
	border_inner_margin = 5,
	border_outer_margin = 0,
	xinerama_head = 0,
	alignment = 'bottom_right',
	gap_x = 10,
	gap_y = 40,
	draw_shades = false,
	draw_outline = false,
	draw_borders = false,
	draw_graph_borders = false,
	use_xft = true,
	font = 'Cascadia Code:size=12',
	xftalpha = 0.8,
	uppercase = false,
	default_color = 'white',
	own_window_colour = '#000000',
	minimum_width = 300, minimum_height = 0,
	alignment = 'top_right',

};
conky.text = [[
${color #f5f50a}${time %H:%M:%S}${alignr}${time %d-%m-%y}
${voffset -16}${font Cascadia Code:bold:size=18}${alignc}${time %H:%M}${font}
${voffset 4}${alignc}${time %A %B %d, %Y}
${font}${voffset -4}
${font Cascadia Code:bold:size=10}${color #9365c2}SYSTEM ${hr 2}$color
${font Cascadia Code:normal:size=8}$sysname $kernel $alignr $machine
Host:$alignr$nodename
Uptime:$alignr$uptime
File System: $alignr${fs_type}
Processes: $alignr ${execi 1000 ps aux | wc -l}

${font Cascadia Code:bold:size=10}${color #9365c2}CPU ${hr 2}$color
${font Cascadia Code:normal:size=8}${execi 1000 grep model /proc/cpuinfo | cut -d : -f2 | tail -1 | sed 's/\s//'}
${font Cascadia Code:normal:size=8}${color #99d115}${cpugraph cpu1}$color
CPU:${color #99d115} ${cpu cpu1}% ${cpubar cpu1}${color}

${font Cascadia Code:bold:size=10}${color #9365c2}MEMORY ${hr 2}$color
${font Cascadia Code:normal:size=8}RAM $alignc $mem / $memmax $alignr $memperc%
${color #99d115}$membar$color
SWAP $alignc ${swap} / ${swapmax} $alignr ${swapperc}%
${color #99d115}${swapbar}$color

${font Cascadia Code:bold:size=10}${color #9365c2} DISK USAGE ${hr 2}$color
${font Cascadia Code:normal:size=8}/ $alignc ${fs_used /} / ${fs_size /} $alignr ${fs_used_perc /}%
${color #99d115}${fs_bar /}$color

${font Cascadia Code:size=11}${color #9365c2}GPU${hr 2}${color}${voffset 2}
${font Cascadia Code:size=9}${color1}GPU Freq.: $alignr ${color}${font}${nvidia gpufreq} Mhz${voffset 3}
${font Cascadia Code:size=9}${color1}Memory Freq.: $alignr ${color}${font}${nvidia memfreq} Mhz${voffset 3}
${font Cascadia Code:size=9}${color1}Temperature: $alignr ${color}${font}${nvidia temp} C ${voffset 3}

${font Cascadia Code:size=10}${color #9365c2}NETWORK ${hr 2}$color
${font Cascadia Code:normal:size=8}Local IPs:${alignr}External IP:
${execi 1000 ip a | grep inet | grep -vw lo | grep -v inet6 | cut -d \/ -f1 | sed 's/[^0-9\.]*//g'}  ${alignr}${execi 1000  wget -q -O- http://ipecho.net/plain; echo}
${font Cascadia Code:normal:size=8}Down: ${downspeed wlp4s0}  ${alignr}Up: ${upspeed wlp4s0} 
${color red}${downspeedgraph wlp4s0 80,130 } ${alignr}${color green}${upspeedgraph wlp4s0 80,130 }$color
${font Cascadia Code:bold:size=10}${color #9365c2}TOP PROCESSES ${hr 2}${color #eb2d92}
${font Cascadia Code:normal:size=8}Name $alignr PID   CPU%   MEM%${font Cascadia Code:bold:size=8}
${top name 1} $alignr ${top pid 1} ${top cpu 1}% ${top mem 1}%
${top name 2} $alignr ${top pid 2} ${top cpu 2}% ${top mem 2}%
${top name 3} $alignr ${top pid 3} ${top cpu 3}% ${top mem 3}%
${top name 4} $alignr ${top pid 4} ${top cpu 4}% ${top mem 4}%
${top name 5} $alignr ${top pid 5} ${top cpu 5}% ${top mem 5}%
${top name 6} $alignr ${top pid 6} ${top cpu 6}% ${top mem 6}%
${top name 7} $alignr ${top pid 7} ${top cpu 7}% ${top mem 7}%
${top name 8} $alignr ${top pid 8} ${top cpu 8}% ${top mem 8}%
${top name 9} $alignr ${top pid 9} ${top cpu 9}% ${top mem 9}%
${top name 10} $alignr ${top pid 10} ${top cpu 10}% ${top mem 10}%
]];
