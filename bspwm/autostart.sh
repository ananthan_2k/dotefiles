#!/bin/bash


##Autostart###
 
killall volumeicon &
killall dunst &
killall polybar &
sleep 1s &&  xrandr --output  HDMI-0 --primary --right-of eDP-1-1 
compton -f &
nitrogen --restore &
$HOME/.config/polybar/bspwm.sh &
dunst &
nm-applet &
volumeicon & 
xrandr --output eDP-1-1 --brightness 0.50 &
xsetroot -cursor_name left_ptr &
