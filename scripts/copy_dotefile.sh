#!/usr/bin/env bash


cp -fR ~/scripts ~/Desktop/git-repos/DoteFiles &
cp -fR ~/.config/awesome ~/Desktop/git-repos/DoteFiles &
cp -fR ~/.config/alacritty ~/Desktop/git-repos/DoteFiles &
cp -fR ~/.config/polybar ~/Desktop/git-repos/DoteFiles &
cp -fR ~/.config/qutebrowser ~/Desktop/git-repos/DoteFiles &
cp -fR ~/.zshrc ~/Desktop/git-repos/DoteFiles &
cp -fR ~/.vimrc ~/Desktop/git-repos/DoteFiles &
cp -fR ~/.conkyrc ~/Desktop/git-repos/DoteFiles &
cp -fR ~/.config/i3  ~/Desktop/git-repos/DoteFiles
cp -fR ~/.config/polybar  ~/Desktop/git-repos/DoteFiles 
echo "################################################################"
echo "##   Copying Dotefiles and config to Dotefile repo is Done    ##"
echo "################################################################"

