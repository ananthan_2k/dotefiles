#!/bin/bash

data=$(date +"%m-%d-%Y-%T")

sleep 0.5 && gnome-screenshot -a -f  ~/Pictures/screenshots/"${data}"_shot.png
