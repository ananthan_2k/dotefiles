# My Linux Setup

Due date: September 25, 2020
Tags: TechGeek
progress: Complete

My journey with Linux has been awesome. I started using your cliche 
Ubuntu with gnome desktop environment. But after getting the hang of using linux, daily driving it, I began deep diving into it. Linux as we know gives as true sense of control over our system. So the first step and major one was to enroll into using window managers. I use Awesome and i3 window managers currently.

I will be writing a post installation script for most of this soon....

All my configuration files will be in gitlab


[DoteFiles](https://gitlab.com/ananthan_2k/dotefiles)


## Post Installation setup and environment

- Start!!
    
    Now I am on of the guys who uses chrome as soon as I install the OS. So now  I will list out this software that I install:
    
    ```bash
    ~ sudo apt-get update && sudo apt-get upgrade
    ```
    
    - Chrome 
        1. The command are : 
        
        ```bash
        $ wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
        ```
        
        ```bash
        $ sudo gdebi google-chrome-stable_current_amd64.deb
        ```
        
    - Git
        
        ```bash
        $ sudo apt install git
        ```
        
    - Zsh
        1. Visit this link for commands : [oh-my-zsh](https://dev.to/nicoh/installing-oh-my-zsh-on-ubuntu-362f).
        2. [starship](https://starship.rs/guide/#%F0%9F%9A%80-installation) prompt and the style template is [here](https://gist.github.com/ryo-ARAKI/48a11585299f9032fa4bda60c9bba593)
        3. zsh like fish [here](https://github.com/abhigenie92/zsh_to_fish)
    - VScode
        1. visit this link: [vscode](https://linuxize.com/post/how-to-install-visual-studio-code-on-ubuntu-20-04/)
        - screenshot of extensions used.
            
            ![My%20Linux%20Setup%209c05d8c4d09d42fbb9a51e80375da3f9/Screenshot_from_2020-09-24_23-42-12.png](screenshots/Screenshot_from_2020-09-24_23-42-12.png)
            
            ![My%20Linux%20Setup%209c05d8c4d09d42fbb9a51e80375da3f9/Screenshot_from_2020-09-24_23-41-55.png](screenshots/Screenshot_from_2020-09-24_23-41-55.png)
            
              
            
        1. Theme used are sythwave and one Dark Pro
    - Install gnome tweaks tool
    - Install material shell extension
    - Telegram desktop
        
        ```bash
        $ sudo apt install telegram-desktop
        ```
        
    - Install Java support
        
        ```bash
        $ sudo apt install openjdk-11-jdk
        ```
        
    - Flutter and dart
        1. Refer this link for installation→ [LINK](https://flutter.dev/docs/get-started/install/linux).
        2. For dart refer this → [LINK](https://dart.dev/get-dart)
    - Discord
    - Slack
    - nala-> a package frontend for apt. [here](https://gitlab.com/volian/nala)
    - Alacritty -Default terminal config in gitlab
    - dmenu - app launcher
    - Cuda for nvidia support.
        - Installing cuda will by nature install nvidia drivers if not exists
        - install [cuda](https://www.how2shout.com/linux/how-to-install-cuda-on-ubuntu-20-04-lts-linux/)
        - This next set of lines are important if its not so automatically!!!
            1. In the file `/usr/share/X11/xorg.conf.d/10-nvidia.conf`, add the line:
            
            ```bash
            Option "PrimaryGPU" "Yes"
            ```
            
            1. In the file `/usr/share/X11/xorg.conf.d/10-amdgpu.conf`, change the line
                
                Change from `Driver "amdgpu"` to  `Driver "modesetting"`
    - wireshark.
        
        For installation refer this site → [LINK](https://computingforgeeks.com/how-to-install-wireshark-on-ubuntu-desktop/)
        
    - Android studio
        
        ```bash
        $ sudo snap install android-studio --classic
        ```
        
    - Vim, plugin file 
    - [Node js](https://linuxize.com/post/how-to-install-node-js-on-ubuntu-20-04/)
    - Material Shell from gnome extension site: [link](https://extensions.gnome.org/extension/3357/material-shell/)

    - Android-sdk
    - Stacer
    - neofetch
    gucharmap - font and glyph viewer
    - htop
    - nvtop(nvidia GPU monitoring tool)
    - scrot - minimal screenshot util
    - zathura - pdf viewer
    - brave browser
    - MailSpring(email client)
    - WPS office
    - Add workspace extensions[ workspace bar]
    - cmus - music player in console
    - vifm - default terminal file manager
    - pcmanfm - default gui file manger
    - lx-appearence - default appearence setter for gtk
    - Nitrogen - Default wallpaper setter
    - Compton - default transparency dependency for wm
    - pulse audio - default audio controller for wm
    - mpv music player
    - pavucontrol
    - sublime-text
    - dunst
    -Forticlient(VPN)
    - polybar - build from source
    - nm-applet
    - volumeicon
    - feh - wallpaper setter from CLI
    - Docker and Docker-compose [here](https://support.netfoundry.io/hc/en-us/articles/360057865692-Installing-Docker-and-docker-compose-for-Ubuntu-20-04)


    ```bash
    #1
    wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
    #2
    echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
    #3
    sudo apt-get update
    sudo apt-get install sublime-text
    #4 set to lauch 
    sudo ln -s /opt/sublime_text/sublime_text /usr/local/bin/subl
    ```
    
    ```bash
    ~ sudo apt-get install stacer
    ```
    
    - [Discord](https://discord.com/)
    - Slack
    - [Anaconda](https://www.anaconda.com/products/individual)  → download and run the shell script for instalation
    - Install ncdu disk manager
    
    ```bash
    $ sudo apt-get install ncdu
    ```
    
    - SSH for both gitlab and github [here](https://medium.com/@viviennediegoencarnacion/manage-github-and-gitlab-accounts-on-single-machine-with-ssh-keys-on-mac-43fda49b7c8d)
    - Insomnia/Postman
    - Webstorm and Pycharm
    - KeypassXC
    - qutebrowser
    - qbittorrent
    - mate-polykit
    - obs
    - flameshot - screenshot
    - Rambox
    - Conky    
    - ferdi(better UI)

    ---

    - SSH config [here](https://medium.com/@viviennediegoencarnacion/manage-github-and-gitlab-accounts-on-single-machine-with-ssh-keys-on-mac-43fda49b7c8d)
    
    ```
    # This is a comment                                
    # Personal github account                                
    Host github.com                                
    HostName github.com                                
    User git                                
    PreferredAuthentications publickey                                
    IdentityFile ~/.ssh/id_rsa_github                                
    # Personal gitlab account                                
    Host gitlab.com                                
    HostName gitlab.com                                
    User bgit                                
    PreferredAuthentications publickey                                
    IdentityFile ~/.ssh/id_rsa                                
     
    ```
    
    ---
    

## window Manager

- awesomewm
- i3
- bspwm


### Awesomewm

- install lua

```bash
sudo apt install lua5.3
```

- Install awesome

```bash
sudo apt install awesome
```

##  i3wm

- install i3
```bash
sudo apt install i3
```

- install i3-gaps from source or from ppa



## Hacks

- For installing fonts download the ttf fonts and unzip and then add it ~/.fonts folder and do :

```bash
fc-cache -vf
```

- To search the apt

```bash
apt-cache search package
```

- To copy the manually installed packages to a file

```bash
apt list --manual-installed | cut -d'/' -f 1 > man-apt-package.txt
```

- unzip for unzipping
- Linking a folder/file to another location to safe resource

```bash
ln -sf target source_path
```

### Possible Error Fixes

- Solving dual monitor detection error, one known solution
    1. In the file `/usr/share/X11/xorg.conf.d/10-nvidia.conf`, add the line:
    
    ```bash
    Option "PrimaryGPU" "Yes"
    ```
    
    1. In the file `/usr/share/X11/xorg.conf.d/10-amdgpu.conf`, change the line
        
        Change from `Driver "amdgpu"` to  `Driver "modesetting"`
        
- Booting issue due to Nvidia
    - Any issues with not being able to boot like this:
    
    ![Untitled](screenshots/nvidia_error.png)
    
    - First remove nvidia completely
    
    ```bash
     sudo apt-get remove --purge '^nvidia-.*'
     sudo apt-get remove --purge '^libnvidia-.*'
     sudo apt-get remove --purge '^cuda-.*
    ```
    
    - Then reinstall again, with what was done above, in post installation part
