syntax on

set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set number relativenumber       " Display line numbers
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set backspace=indent,eol,start
set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=lightgrey

call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox'
Plug 'jremmen/vim-ripgrep'
Plug 'tpope/vim-fugitive'
Plug 'leafgarland/typescript-vim'
Plug 'vim-utils/vim-man'
Plug 'lyuts/vim-rtags'
Plug 'https://github.com/ctrlpvim/ctrlp.vim.git'
"Plug 'https://github.com/ycm-core/YouCompleteMe.git'
Plug 'https://github.com/airblade/vim-gitgutter.git' "gitgutter git support
Plug 'mbbill/undotree'                            
Plug 'editorconfig/editorconfig-vim'                 "Editor config 
Plug 'https://github.com/preservim/tagbar.git' 
Plug 'preservim/nerdtree'                            "NERDTree  
Plug 'itchyny/lightline.vim'                         "Lightline status     
Plug 'PotatoesMaster/i3-vim-syntax'                  "i3 config highlighting
Plug 'ap/vim-css-color'                              "css syntax highlight 
Plug 'joshdick/onedark.vim'                          "One dark theme
Plug 'dracula/vim', { 'as': 'dracula' }              "dracula theme 
Plug 'neoclide/coc.nvim', {'branch': 'release'}      "coc autocomplete
call plug#end()

"filetype plugin indent on    " required
au BufEnter,BufRead config setf dosini
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
let g:lightline = {
      \ 'colorscheme': 'one',
      \ }

colorscheme onedark
set background=dark
hi! Normal ctermbg=NONE guibg=NONE
hi!  LineNr ctermfg=blue 
set laststatus=2
set noshowmode
set wildmode=longest,list,full
set wildmenu					" Display all matches when tab complete.
"au BufNewFile,BufRead *.ini

let mapleader = " "
let g:netrw_browse_split=2
let g:netrw_banner=0
let g:netrw_winsize = 25

let g:ctrlp_use_caching = 0

nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>u :UndotreeShow<CR>
nnoremap <leader>po :wincmd v<bar> :Ex <bar> :vertical resize 30<CR>
nnoremap <leader>+ :vertical resize +5<CR>
nnoremap <leader>- :vertical resize -5<CR>
nnoremap <leader>n :NERDTreeToggle<CR>
nnoremap <silent> gd <Plug>(coc-definition)
nnoremap <silent> gf <Plug>(coc-type-definition)
nnoremap <leader>c :CtrlP<CR>

" Start NERDTree and leave the cursor in it.
autocmd VimEnter * NERDTree | wincmd p

highlight GitGutterAdd guibg=#009900 ctermbg=Green
highlight GitGutterChange guibg=#bbbb00 ctermbg=Yellow
highlight GitGutterAdd guibg=#ff2222 ctermbg=Red
let g:gitgutter_enabled = 1
let g:gitgutter_map_keys = 0
